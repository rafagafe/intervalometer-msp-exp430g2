#
# Generated Makefile - do not edit!
#
# Edit the Makefile in the project folder instead (../Makefile). Each target
# has a -pre and a -post target defined where you can add customized code.
#
# This makefile implements configuration specific macros and targets.


# Environment
MKDIR=mkdir
CP=cp
GREP=grep
NM=nm
CCADMIN=CCadmin
RANLIB=ranlib
CC=msp430-gcc
CCC=msp430-g++
CXX=msp430-g++
FC=gfortran
AS=msp430-as

# Macros
CND_PLATFORM=msp430-Linux
CND_DLIB_EXT=so
CND_CONF=Release
CND_DISTDIR=dist
CND_BUILDDIR=build

# Include project Makefile
include Makefile

# Object Directory
OBJECTDIR=${CND_BUILDDIR}/${CND_CONF}/${CND_PLATFORM}

# Object Files
OBJECTFILES= \
	${OBJECTDIR}/_ext/511e4115/date-time.o \
	${OBJECTDIR}/_ext/511e4115/fifo.o \
	${OBJECTDIR}/_ext/511e4115/flash.o \
	${OBJECTDIR}/_ext/511e4115/intervalometer.o \
	${OBJECTDIR}/_ext/511e4115/main.o \
	${OBJECTDIR}/_ext/511e4115/prompt.o \
	${OBJECTDIR}/_ext/511e4115/rtc.o \
	${OBJECTDIR}/_ext/511e4115/scheduler.o \
	${OBJECTDIR}/_ext/511e4115/switch.o \
	${OBJECTDIR}/_ext/511e4115/terminal.o \
	${OBJECTDIR}/_ext/511e4115/timer.o


# C Compiler Flags
CFLAGS=-mmcu=msp430g2553 -ffunction-sections

# CC Compiler Flags
CCFLAGS=-mmcu=msp430g2553
CXXFLAGS=-mmcu=msp430g2553

# Fortran Compiler Flags
FFLAGS=

# Assembler Flags
ASFLAGS=

# Link Libraries and Options
LDLIBSOPTIONS=

# Build Targets
.build-conf: ${BUILD_SUBPROJECTS}
	"${MAKE}"  -f nbproject/Makefile-${CND_CONF}.mk ${CND_DISTDIR}/${CND_CONF}/intervalometer.elf

${CND_DISTDIR}/${CND_CONF}/intervalometer.elf: ${OBJECTFILES}
	${MKDIR} -p ${CND_DISTDIR}/${CND_CONF}
	${LINK.c} -o ${CND_DISTDIR}/${CND_CONF}/intervalometer.elf ${OBJECTFILES} ${LDLIBSOPTIONS} -Wl,--gc-sections

${OBJECTDIR}/_ext/511e4115/date-time.o: ../src/date-time.c 
	${MKDIR} -p ${OBJECTDIR}/_ext/511e4115
	${RM} "$@.d"
	$(COMPILE.c) -O3 -Wall -D__MSP430G2553__ -std=c99 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/511e4115/date-time.o ../src/date-time.c

${OBJECTDIR}/_ext/511e4115/fifo.o: ../src/fifo.c 
	${MKDIR} -p ${OBJECTDIR}/_ext/511e4115
	${RM} "$@.d"
	$(COMPILE.c) -O3 -Wall -D__MSP430G2553__ -std=c99 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/511e4115/fifo.o ../src/fifo.c

${OBJECTDIR}/_ext/511e4115/flash.o: ../src/flash.c 
	${MKDIR} -p ${OBJECTDIR}/_ext/511e4115
	${RM} "$@.d"
	$(COMPILE.c) -O3 -Wall -D__MSP430G2553__ -std=c99 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/511e4115/flash.o ../src/flash.c

${OBJECTDIR}/_ext/511e4115/intervalometer.o: ../src/intervalometer.c 
	${MKDIR} -p ${OBJECTDIR}/_ext/511e4115
	${RM} "$@.d"
	$(COMPILE.c) -O3 -Wall -D__MSP430G2553__ -std=c99 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/511e4115/intervalometer.o ../src/intervalometer.c

${OBJECTDIR}/_ext/511e4115/main.o: ../src/main.c 
	${MKDIR} -p ${OBJECTDIR}/_ext/511e4115
	${RM} "$@.d"
	$(COMPILE.c) -O3 -Wall -D__MSP430G2553__ -std=c99 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/511e4115/main.o ../src/main.c

${OBJECTDIR}/_ext/511e4115/prompt.o: ../src/prompt.c 
	${MKDIR} -p ${OBJECTDIR}/_ext/511e4115
	${RM} "$@.d"
	$(COMPILE.c) -O3 -Wall -D__MSP430G2553__ -std=c99 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/511e4115/prompt.o ../src/prompt.c

${OBJECTDIR}/_ext/511e4115/rtc.o: ../src/rtc.c 
	${MKDIR} -p ${OBJECTDIR}/_ext/511e4115
	${RM} "$@.d"
	$(COMPILE.c) -O3 -Wall -D__MSP430G2553__ -std=c99 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/511e4115/rtc.o ../src/rtc.c

${OBJECTDIR}/_ext/511e4115/scheduler.o: ../src/scheduler.c 
	${MKDIR} -p ${OBJECTDIR}/_ext/511e4115
	${RM} "$@.d"
	$(COMPILE.c) -O3 -Wall -D__MSP430G2553__ -std=c99 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/511e4115/scheduler.o ../src/scheduler.c

${OBJECTDIR}/_ext/511e4115/switch.o: ../src/switch.c 
	${MKDIR} -p ${OBJECTDIR}/_ext/511e4115
	${RM} "$@.d"
	$(COMPILE.c) -O3 -Wall -D__MSP430G2553__ -std=c99 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/511e4115/switch.o ../src/switch.c

${OBJECTDIR}/_ext/511e4115/terminal.o: ../src/terminal.c 
	${MKDIR} -p ${OBJECTDIR}/_ext/511e4115
	${RM} "$@.d"
	$(COMPILE.c) -O3 -Wall -D__MSP430G2553__ -std=c99 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/511e4115/terminal.o ../src/terminal.c

${OBJECTDIR}/_ext/511e4115/timer.o: ../src/timer.c 
	${MKDIR} -p ${OBJECTDIR}/_ext/511e4115
	${RM} "$@.d"
	$(COMPILE.c) -O3 -Wall -D__MSP430G2553__ -std=c99 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/511e4115/timer.o ../src/timer.c

# Subprojects
.build-subprojects:

# Clean Targets
.clean-conf: ${CLEAN_SUBPROJECTS}
	${RM} -r ${CND_BUILDDIR}/${CND_CONF}
	${RM} ${CND_DISTDIR}/${CND_CONF}/intervalometer.elf

# Subprojects
.clean-subprojects:

# Enable dependency checking
.dep.inc: .depcheck-impl

include .dep.inc
