#
# Generated - do not edit!
#
# NOCDDL
#
CND_BASEDIR=`pwd`
CND_BUILDDIR=build
CND_DISTDIR=dist
# Debug configuration
CND_PLATFORM_Debug=msp430-Linux
CND_ARTIFACT_DIR_Debug=dist/Debug
CND_ARTIFACT_NAME_Debug=intervalometer.elf
CND_ARTIFACT_PATH_Debug=dist/Debug/intervalometer.elf
CND_PACKAGE_DIR_Debug=dist/Debug/msp430-Linux/package
CND_PACKAGE_NAME_Debug=netbeans-project.tar
CND_PACKAGE_PATH_Debug=dist/Debug/msp430-Linux/package/netbeans-project.tar
# Release configuration
CND_PLATFORM_Release=msp430-Linux
CND_ARTIFACT_DIR_Release=dist/Release
CND_ARTIFACT_NAME_Release=intervalometer.elf
CND_ARTIFACT_PATH_Release=dist/Release/intervalometer.elf
CND_PACKAGE_DIR_Release=dist/Release/msp430-Linux/package
CND_PACKAGE_NAME_Release=netbeans-project.tar
CND_PACKAGE_PATH_Release=dist/Release/msp430-Linux/package/netbeans-project.tar
# Simulation configuration
CND_PLATFORM_Simulation=msp430-Linux
CND_ARTIFACT_DIR_Simulation=dist/Simulation
CND_ARTIFACT_NAME_Simulation=intervalometer.elf
CND_ARTIFACT_PATH_Simulation=dist/Simulation/intervalometer.elf
CND_PACKAGE_DIR_Simulation=dist/Simulation/msp430-Linux/package
CND_PACKAGE_NAME_Simulation=netbeans-project.tar
CND_PACKAGE_PATH_Simulation=dist/Simulation/msp430-Linux/package/netbeans-project.tar
#
# include compiler specific variables
#
# dmake command
ROOT:sh = test -f nbproject/private/Makefile-variables.mk || \
	(mkdir -p nbproject/private && touch nbproject/private/Makefile-variables.mk)
#
# gmake command
.PHONY: $(shell test -f nbproject/private/Makefile-variables.mk || (mkdir -p nbproject/private && touch nbproject/private/Makefile-variables.mk))
#
include nbproject/private/Makefile-variables.mk
