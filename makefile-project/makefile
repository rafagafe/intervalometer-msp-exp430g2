NAME            = intervalometer
OBJECTS         = date-time.o fifo.o flash.o intervalometer.o main.o prompt.o rtc.o scheduler.o switch.o terminal.o timer.o
SRCS 						= date-time.c fifo.c flash.c intervalometer.c main.c prompt.c rtc.c scheduler.c switch.c terminal.c timer.c
CPU             = msp430g2553
CFLAGS          = -mmcu=${CPU} -ffunction-sections -O2 -Wall -g -std=c99
CC							= msp430-gcc
DRIVER          = rf2500
VPATH           = ../src/

.PHONY: build all FORCE clean debug dist sim terminal help 
	
build: ${NAME}.elf

all: clean ${NAME}.elf ${NAME}.a43 ${NAME}.lst

clean:
	rm -f ${NAME}.elf ${NAME}.a43 ${NAME}.lst ${OBJECTS} script.gdb core
	clear

sim: ${NAME}.elf script.gdb
	mspdebug sim "prog ${NAME}.elf" gdb &
	msp430-gdb -x script.gdb "${NAME}.elf"
	rm -f script.gdb core	
	
debug: ${NAME}.elf script.gdb
	mspdebug ${DRIVER} gdb & msp430-gdb ${NAME}.elf -x script.gdb
	rm -f script.gdb 	

run: ${NAME}.elf
	mspdebug ${DRIVER} "prog ${NAME}.elf"
	
term: run	
	minicom -D /dev/ttyACM0 -b 9600

help:
	@echo "Targets:"
	@echo "\t-build (default)"
	@echo "\t-all"
	@echo "\t-clean"
	@echo "\t-debug"
	@echo "\t-sim"
	@echo "\t-run"
	@echo "\t-term"
	@echo "\t-help"
	
${NAME}.elf: ${OBJECTS} dependencies.mk
	${CC} -mmcu=${CPU} -Wl,--gc-sections -o $@ ${OBJECTS}

${NAME}.a43: ${NAME}.elf
	msp430-objcopy -O ihex $^ $@

${NAME}.lst: ${NAME}.elf
	msp430-objdump -dSt $^ >$@

script.gdb:
	echo "target remote localhost:2000\nerase all\nload\nbreak main\ncontinue" > script.gdb

dependencies.mk: ${SRCS}
	${CC} ${CFLAGS} -MM $^ > dependencies.mk

include dependencies.mk


