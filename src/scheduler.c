
/* ------------------------------------------------------------------------ */
/** @file  scheduler.c
  * @brief Scheduler module.
  * @date   20/09/2015
  * @author Rafa García.                                                    */
/* ------------------------------------------------------------------------ */

#include <stdbool.h>
#include <msp430.h>
#include <intrinsics.h>
#include "scheduler.h"


/* Tell to the scheduler that there is a new pending event. */
static volatile bool _wakeup;

void scheduler_wakeup( void ) { _wakeup = true; }

__attribute__((noreturn))
void scheduler_run( thread_t const* const pool ) {
    
    _wakeup = false; 
    
    for(;;) {

        for( thread_t const *restrict run = pool; *run; ++run )
            while ( _wakeup || ( BLOCKED != (*run)() ) ) { 
                run = pool;
                _wakeup = false;
            }                          

        __dint();
        while( !_wakeup ) {
            __bis_status_register( LPM0_bits | GIE );
            __dint();
        }
        __eint();

    }
}

/* ------------------------------------------------------------------------ */
