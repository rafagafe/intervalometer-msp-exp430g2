
/* ------------------------------------------------------------------------ */
/** @file prompt.h
  * @brief Interface Prompt module.
  * @date 04/01/2014.
  * @author   Rafa García.                                                  */
/* ------------------------------------------------------------------------ */

#ifndef _PROMPT_
#define _PROMPT_

#include <stdbool.h>
#include "scheduler.h"

/** @defgroup prompt Prompt @{ */

/** This task executes commands line. */
state_t prompt_task( void );

/** @ } */

#endif /* _PROMPT_ */
