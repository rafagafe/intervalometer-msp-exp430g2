
/* ------------------------------------------------------------------------ */
/** @file prompt.c
  * @brief This module implements a tasks that emulates a RTC. 
  *        Also methods that pass messages to this tasks.
  * @date 04/01/2014.
  * @author Rafa García.                                                    */
/* ------------------------------------------------------------------------ */
                                                                              
#include "rtc.h"
#include "timer.h"
#include "flash.h"

static void _setDefault( void );

/** Current date time register. */
static dateTime_t _rtc;
/** Alarm register. */
static dateTime_t _alarm;
/** Indicate that RTC is running or not. */
static bool _run;
/** Indicate if alarm is active. */
static bool _alarmOn;
/** Process runned when alarm event. */
static process_t _process;

/* Programs running intervalometer process when alarm event. */
void rtc_setAlarmProcess( process_t p ) { _process = p; }

/* Get the intervalometer process runned when alarm event. */
process_t rtc_getAlarmProcess( void ) { return _process; }

/* Setup and enable the RTC. */
bool rtc_set( dateTime_t const* newDateTime ) {
    if ( !dateTime_check( newDateTime ) ) return false;    
    _rtc = *newDateTime;
    return _run = true;
}

/* Get current date and time. */
void rtc_get( dateTime_t* dateTime ) { *dateTime = _rtc; }

/* Set and enable alarm. */
bool rtc_setAlarm( dateTime_t const* newAlarm ) {
    if ( !dateTime_check( newAlarm ) ) return false;
    _alarm = *newAlarm;    
    return _alarmOn = true;
}

/* Get current alarm register. */
void rtc_getAlarm( dateTime_t* alarm ) { *alarm = _alarm; }

/* Enable or disable RTC task. For power saving. */
void rtc_enable( bool en ) { _run = en; }

/* Enable or disable alarm. */
void rtc_alarm( bool en ) { _alarmOn = en; }

/* This tasks that emulates a RTC. */
state_t rtc_task( void ) {
    static lc_t lc = 0;
    static timer_t t;
    begin( lc );
    _setDefault();
    _alarmOn = false;
    _run = false;
    _process = flash_getAlarmProcess();
    while( true ) {
        waitUntil( lc, _run );
        timer_go( true );
        t = timer_getTimeStamp();
        do {
            waitUntil( lc, timer_isPeriodOver( &t, __sec(1.0) ) );
            dateTime_incSec( &_rtc );
            if ( _alarmOn && dateTime_isGreaterOrEqual( &_rtc, &_alarm ) ) {
                intervalometer_run( _process );
                _alarmOn = false;                
            }
        } while( _run );
        timer_go( false );
    }    
    end( lc );
}

/** Set alarm and rtc register to default values. */
static void _setDefault( void ) {
    static dateTime_t const defaultAlarm = {
        .century = 21, .year = 14, .month = january, 
        .day = 1, .hour = 0, .min = 0, .sec = 0
        
    };
    _alarm = defaultAlarm;
}

/* ------------------------------------------------------------------------ */
