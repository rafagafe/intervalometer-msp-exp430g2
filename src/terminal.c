
/* ------------------------------------------------------------------------ */
/** @file terminal.c
  * @brief This module implements two tasks that communicate with a 
  *        terminal via serial port. Also methods that pass messages 
  *        to these tasks to make input-output functions.
  * @date   10/02/2015.
  * @author Rafa García.                                                    */
/* ------------------------------------------------------------------------ */

#include <msp430.h>
#include <intrinsics.h>
#include "terminal.h"
#include "fifo.h"

/** Length in bytes of output queue. */
#define _OUTPUT_SIZE 8

/** Defines the type of commands. */
typedef enum __attribute( ( packed ) ) command_e { 
    NOTHING = 0, /**< Nothing to do. */        
    STRING  = 1, /**< Receive a string.*/  
    NUMERIC = 2, /**< Receive a numeric string.*/
    HEXA    = 3, /**< Receive a hexa digits string. */    
} rxCommand_t;

/** Commands that tx-task gets from output queue. */
typedef enum __attribute( ( packed ) ) {
    PTR, /**< Pointer to string.            */
    STR, /**< String in output queue.       */
    CHR, /**< Single char in output queue.  */
    BS,  /**< Back spaces sequence.         */        
    X8,  /**< Byte in hex format.           */
    X16, /**< Word in hex format.           */
    S16, /**< Signed word in dec format.    */
    U16, /**< Unsigned word in dec format.  */            
} txCommand_t;



/* ------------------------------------------------------------------------ */
/* ----------------------------------------------- Internal prototypes: --- */
/* ------------------------------------------------------------------------ */
static void _config( void );
static bool _send( uint8_t byte );
static bool _receive( uint8_t *byte );
static char _nibble( uint8_t n );
static bool _isPrintable( rxCommand_t command, char ch );
static bool _char( char ch );
static bool _msg( const char *msg );
static bool _endl( void );
static bool _get( char *string, uint16_t length, rxCommand_t command  );



/* ------------------------------------------------------------------------ */
/* ---------------------------------------------------- State vaiables: --- */
/* ------------------------------------------------------------------------ */
/** Indicates if this driver has been reserved. */
static bool _open = true;
/** Command to do. */
static rxCommand_t _rxCommand = NOTHING;
/** Pointer to user buffer. */
static char *_string;
/** Length of user buffer. */
static uint16_t _length;
/** Data space for output queue. */
static uint8_t _output_data[_OUTPUT_SIZE];
/** Output queue. */
static fifo_t _output = { 0, 0, 0, _OUTPUT_SIZE, _output_data };

static char _lastChar = 0;

/* ------------------------------------------------------------------------ */
/* -------------------------------------------------- Public functions: --- */
/* ------------------------------------------------------------------------ */
/* It tries to open the terminal: */
bool terminal_open( void ) {
    if ( _open ) return false;
    return _open = true;
}

/* It closes the terminal if was open: */
void terminal_close( void ) { _open = false; }

bool terminal_getChar( char* ch ) {
    if ( terminal_isBusy() ) return false;
    if ( _lastChar == '\0') return false;
    *ch = _lastChar;
    _lastChar = '\0';
    return true;
}

void terminal_flush( void ) { _lastChar = '\0'; }

/* Check if terminal rx task is working. */
bool terminal_isBusy( void ) { return ( _rxCommand != NOTHING ); }


/* It passes a message to terminal task to get a string: */
bool terminal_getString( char *string, uint16_t length ) {
    return _get( string, length, STRING  );  
}

/* It passes a message to terminal task to get a numerical string: */
bool terminal_getNumeric( char *string, uint16_t length ) {
    return _get( string, length, NUMERIC  );  
}

/* It passes a message to terminal task to get a hexa digits string. */
bool terminal_getHex( char *string, uint16_t length ) {
    return _get( string, length, HEXA  ); 
}


state_t terminal_rxTask( void ) {  
    static lc_t lc = 0;
    static char ch;   
    static uint8_t i; 
    begin( lc );            
    _config();
    fifo_flush( &_output );  
    _open = false;
    while( true ) {
        _rxCommand = NOTHING;
        do {
            waitWhile( lc, _receive( (uint8_t*)&_lastChar ) || ( _rxCommand == NOTHING ) );
        } while (_rxCommand == NOTHING);
        _lastChar = '\0';
        i = 0;
        do {
            waitUntil( lc, _receive( (uint8_t*)&ch ) );
            bool printable = _isPrintable( _rxCommand, ch );
            if ( ( i < (_length-1) ) && printable ) {
                _string[i++] = ch;
                waitUntil( lc, _char( ch ) );
            }    
            else if ( ch == '\b' ) {
                if ( i ) {
                    i--;
                    waitUntil( lc, _msg("\b \b") ); 
                }
            }
            else if ( (ch == '\r') || (ch == '\n') ) {
                _string[i] = ch = 0; 
                waitUntil( lc, _endl() );
            }
            else
                waitUntil( lc, _char('\a') );
        } while( ch );
    }   
    end( lc );
}


/* This task gets data from an output queue, it
                             formats them to text and sends by serial port. */
state_t terminal_txTask( void ) {
    static lc_t lc = 0;
    static const uint16_t div[] = { 10000, 1000, 100, 10 };
    static char* ptr;
    static union { 
        uint16_t w;
        uint8_t b;
    } data;    
    static bool firstDigit;    
    static char charAux;
    static uint8_t i;    
    begin( lc );            
    while( true ) {              
        waitUntil( lc, fifo_get( &_output, &data.b ) );        
        switch( (txCommand_t)data.b ) {
            case PTR: {                
                for( ptr = fifo_getPtr( &_output ); *ptr; ptr++ )
                    waitUntil( lc, _send( *ptr ) );
                break;
            }
            case STR: {
                fifo_get( &_output, &data.b );
                while( data.b ) {
                    waitUntil( lc, _send( *ptr ) );
                    fifo_get( &_output, &data.b );
                }
                break;
            }          
            case CHR: {
                fifo_get( &_output, &data.b );
                waitUntil( lc, _send( data.b ) );
                break;
            }
            case BS: {
                fifo_get( &_output, &data.b );
                for( i = 0; i < data.b; i++ )
                    waitUntil( lc, _send( '\b' ) );
                for( i = 0; i < data.b; i++ )
                    waitUntil( lc, _send( ' ' ) );
                for( i = 0; i < data.b; i++ )
                    waitUntil( lc, _send( '\b' ) );
                break;
            }
            case X16: {
                fifo_get( &_output, &data.b );
                charAux = _nibble( data.b >> 4 );
                waitUntil( lc, _send( charAux ) );
                charAux = _nibble( data.b );
                waitUntil( lc, _send( charAux ) );
            }             
            case X8: {
                fifo_get( &_output, &data.b );
                charAux = _nibble( data.b >> 4 );
                waitUntil( lc, _send( charAux ) );
                charAux = _nibble( data.b );
                waitUntil( lc, _send( charAux ) );
                break;
            }           
            case S16:
                waitUntil( lc, _send( '-' ) );                 
            case U16: {                    
                fifo_get( &_output, &((uint8_t*)&data.w)[1] );
                fifo_get( &_output, &((uint8_t*)&data.w)[0] );                
                firstDigit = false;
                for( i = 0; i < 4; i++ ) {
                    if ( ( data.w >= div[i] ) || firstDigit ) {
                        charAux = (data.w / div[i]) + '0';
                        yield( lc );
                        waitUntil( lc, _send( charAux ) );
                        firstDigit = true;
                    }
                    data.w = data.w % div[i];
                    yield( lc );
                }
                charAux = (data.w % 10) + '0';
                yield( lc );
                waitUntil( lc, _send( charAux ) );              
                break;
            }                
            default: fifo_flush( &_output );            
        }   
    }  
    end( lc );
}

/* Try to put a pointer to string in output queue. */
bool terminal_str( const char *msg ) {
    if ( terminal_isBusy() ) return false;
    return _msg( msg );
}

/* Try to put a char in output queue. */
bool terminal_char( char ch ) {
    if ( terminal_isBusy() ) return false;
    return _char( ch );
}

bool terminal_backSpaces( uint8_t qty ) {
    if ( terminal_isBusy() ) return false;
    if ( fifo_getSpaceAvilable( &_output ) < 2 ) return false;    
    fifo_put( &_output, BS );
    fifo_put( &_output, qty );  
    return true;
}
/* Try to put a end of line in output queue. */
bool terminal_endl( void ) { 
    if ( terminal_isBusy() ) return false;
    return _endl();   
}

/* Try to put a byte in output queue to be printed in hex format. */
bool terminal_x8( uint8_t byte ) {
    if ( terminal_isBusy() ) return false;
    if ( fifo_getSpaceAvilable( &_output ) < 2 ) return false;    
    fifo_put( &_output, X8 );
    fifo_put( &_output, byte );
    return true;
}

/* Try to put a word in output queue to be printed in hex format. */
bool terminal_x16( uint16_t word ) {
    if ( terminal_isBusy() ) return false;
    if ( fifo_getSpaceAvilable( &_output ) < 3 ) return false;    
    fifo_put( &_output, X16 );
    fifo_put( &_output, ((uint8_t*)&word)[1] );
    fifo_put( &_output, ((uint8_t*)&word)[0] );
    return true;
}

/* Try to put a unsigned word in output queue to be printed in dec format. */
bool terminal_u16( uint16_t word ) {
    if ( terminal_isBusy() ) return false;
    if ( fifo_getSpaceAvilable( &_output ) < 3 ) return false;    
    fifo_put( &_output, U16 ); 
    fifo_put( &_output, ((uint8_t*)&word)[1] );
    fifo_put( &_output, ((uint8_t*)&word)[0] );
    return true;    
}

/* Try to put a signed word in output queue to be printed in dec format. */
bool terminal_s16( int16_t word ) {
    if ( terminal_isBusy() ) return false;
    if ( fifo_getSpaceAvilable( &_output ) < 3 ) return false;     
    if ( word < 0 ) {
        word = -word;
        fifo_put( &_output, S16 ); 
    }
    else fifo_put( &_output, U16 ); 
    fifo_put( &_output, ((uint8_t*)&word)[1] );
    fifo_put( &_output, ((uint8_t*)&word)[0] );    
    return true;
}

/* Try to put a unsigned byte in output queue to be printed in dec format. */
bool terminal_u8( uint8_t byte ) { return terminal_u16( byte ); }

/* Try to put a signed byte in output queue to be printed in dec format. */
bool terminal_s8( int8_t byte ) { return terminal_s16( byte ); }



/* ------------------------------------------------------------------------ */
/* -------------------------------------------------- Private funtions: --- */
/* ------------------------------------------------------------------------ */
/** USCIA0 TX ISR. */
__attribute__( ( __interrupt__( USCIAB0TX_VECTOR ), wakeup ) )
static void _usciAB0_tx_isr( void ) {
    IE2 &= ~UCA0TXIE;
    scheduler_wakeup();        
}


/** USCIA0 RX ISR. */
__attribute__( ( __interrupt__( USCIAB0RX_VECTOR ), wakeup ) )
static void _usciAB0_rx_isr( void ) {
    IE2 &= ~UCA0RXIE;
    scheduler_wakeup();   
}

/** Configure hardware and driver state variav¡bles. */
static void _config( void ) {    
    UCA0CTL1 |= UCSWRST;  
    P1SEL  |= BIT1 | BIT2;  // P1.1 <-> RXD, P1.2 <-> TXD
    P1SEL2 |= BIT1 | BIT2;  // P1.1 <-> RXD, P1.2 <-> TXD
    UCA0CTL1 |= UCSSEL_2;   // Use SMCLK    
    UCA0BR0 = 104;          // Baudrate to 9600 with 1MHz clock
    UCA0BR1 = 0;   
    UCA0MCTL = UCBRS0;      // Modulation UCBRSx = 1
    UCA0CTL1 &= ~UCSWRST;   // Initialize USCI state machine    
    IE2 |= UCA0RXIE;
}


/** Sends a byte if possible by the UART.
  * @param data: Byte to be sent.
  * @return false while is not possible send bytes. True in other case. */
static bool _send( uint8_t byte ) {
    if ( IFG2 & UCA0TXIFG ) {
        UCA0TXBUF = byte;
        IE2 |= UCA0TXIE;
        return true;
    }
    return false;
}

/** Get if any byte received by the UART.
  * @param data[out]: Received byte.
  * @return False until there is a received byte. True in other case. */
static bool _receive( uint8_t *byte ) {
    if ( IFG2 & UCA0RXIFG ) {
        *byte = UCA0RXBUF;
        IE2 |= UCA0RXIE;
        return true;
    }
    return false;
}


/** Get the low nibble of byte coded in text. */
static char _nibble( uint8_t n ) { return "0123456789ABCDEF"[ n & 0xf ]; }

/** It passes a message to terminal task.
  * @param string: Destination of string.
  * @param length: Max. length of string buffer.
  * @param command: Action to do.
  * @retval false while the message is not still passed.
  * @retval true when the message is passed successfully. */
static bool _get( char *string, uint16_t length, rxCommand_t command  ) {
    /* If not open returns null string: */
    if ( !_open ) { 
        *string = '\0'; 
        return true; 
    }
    /* If it's doing nothing starts process: */
    if ( _rxCommand == NOTHING ) {        
        _rxCommand = command;
        _string = string;
        _length = length;
        return true;
    }
    /* The tasks is busy: */
    return false;  
}

/** Check if a chracter is printable for a command.
  * @param command: scope.
  * @param ch: Character to exam.
  * @retval false non printable.
  * @retval true printable. */
static bool _isPrintable( rxCommand_t command, char ch ) {
    bool retVal = false;
    switch( command ) {
        case NUMERIC: 
            retVal = ( ( ch >= '0' ) && ( ch <= '9' ) );
            break;
        case HEXA: 
            retVal  = ( ( ch >= '0' ) && ( ch <= '9' ) );
            retVal |= ( ( ch >= 'A' ) && ( ch <= 'F' ) );
            retVal |= ( ( ch >= 'a' ) && ( ch <= 'f' ) );
            break;
        default: 
            retVal = ( ch >= ' ' );            
    } 
    return retVal;
}

/* Try to put a pointer to string in output queue. */
static bool _msg( const char *msg ) {
    if ( fifo_getSpaceAvilable( &_output ) < 3 ) return false;
    fifo_put( &_output, PTR );
    fifo_putPtr( &_output, (void*)msg );
    return true;
}

/* Try to put a char in output queue. */
static bool _char( char ch ) {
    if ( fifo_getSpaceAvilable( &_output ) < 2 ) return false;    
    fifo_put( &_output, CHR );
    fifo_put( &_output, ch );  
    return true;
}

/* Try to put a end of line in output queue. */
static bool _endl( void ) { 
#if 1
    return _msg("\n\r");
#else
    return _char('\n');
#endif    
}

/* ------------------------------------------------------------------------ */
