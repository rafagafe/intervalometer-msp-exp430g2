
/* ------------------------------------------------------------------------ */
/** @file main.c
  * @brief 
  * @date   10/02/2015.
  * @author Rafa García.                                                    */
/* ------------------------------------------------------------------------ */

#include <msp430.h>
#include <intrinsics.h>
#include "timer.h"
#include "scheduler.h"
#include "terminal.h"
#include "switch.h"
#include "intervalometer.h"
#include "prompt.h"
#include "rtc.h"

/** Task pool for the scheduler. In priority order. */
const thread_t _pool[] = { 
    intervalometer_task,    /**< highest priority */  
    terminal_rxTask,        
    rtc_task,
    switch_task, 
    terminal_txTask, 
    prompt_task,            /**< lowest priority */    
    0
};

/** Entry point of application. */
int main( void ) {    
    /* Config watch dog timer: */
    WDTCTL = WDTPW | WDTHOLD;
    BCSCTL1 = CALBC1_1MHZ; 
    DCOCTL = CALDCO_1MHZ;          
    __eint();    
    /** Config common driver used by serveral tasks:*/
    timer_config();    
    /** Run scheduler: */
    scheduler_run( _pool );    
    return 0;   
} 

/* ------------------------------------------------------------------------ */
