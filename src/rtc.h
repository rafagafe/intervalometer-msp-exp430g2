
/* ------------------------------------------------------------------------ */
/** @file prompt.h
  * @brief Interface to RTC module.
  * @date 04/01/2014.
  * @author   Rafa García.                                                  */
/* ------------------------------------------------------------------------ */

#ifndef _RTC_
#define	_RTC_

#include "date-time.h"
#include "scheduler.h"
#include "intervalometer.h"

/** @defgroup rtc Real Time Calendar
  * This module implements a tasks that emulates a RTC. Also methods that pass 
  * messages to this tasks. @{  */

/** This tasks that emulates a RTC. */
state_t rtc_task( void );

/** Enable or disable RTC task. For power saving. */
void rtc_enable( bool en );

/** Enable or disable alarm. */
void rtc_alarm( bool en );

/** Setup and enable the RTC. */
bool rtc_set( dateTime_t const* newDateTime );

/** Get current date and time. */
void rtc_get( dateTime_t* dateTime );

/** Set and enable alarm. */
bool rtc_setAlarm( dateTime_t const* newAlarm );

/** Get current alarm register. */
void rtc_getAlarm( dateTime_t* alarm );

/** Programs running intervalometer process when alarm event. */
void rtc_setAlarmProcess( process_t p );

/** Get the intervalometer process runned when alarm event. */
process_t rtc_getAlarmProcess( void );
        
/** @ } */

#endif	/* _RTC_ */

