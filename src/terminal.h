
/* ------------------------------------------------------------------------ */
/** @file  terminal.h
  * @brief Interface to terminal module.
  * @date   20/09/2015
  * @author Rafa García.                                                    */
/* ------------------------------------------------------------------------ */

#ifndef _TERMINAL_
#define	_TERMINAL_

#include <stdbool.h>
#include <stdint.h>
#include "scheduler.h"

/** @defgroup terminal Terminal
  * This module implements two tasks that communicate with a terminal 
  * via serial port. Also methods that pass messages to these tasks to 
  * make input-output functions. @{  */


/* ------------------------------------------------------------------------ */
/* ----------------------------------------------------------- Control: --- */
/* ------------------------------------------------------------------------ */

/** It tries to open the terminal.
  * @retval true when the terminal has been open successfully.
  * @retval false while the terminal cannot be open.*/
bool terminal_open( void );
#define terminal_wOpen( lc ) waitUntil( lc, terminal_open() )

/** It closes the terminal if was open. */
void terminal_close( void );



/* ------------------------------------------------------------------------ */
/* ----------------------------------------------------------- Receive: --- */
/* ------------------------------------------------------------------------ */

/** This task receives bytes from uart and puts an input queue. */
state_t terminal_rxTask( void );

bool terminal_getChar( char* ch );
#define terminal_wGetChar( lc, ch ) waitUntil( lc, terminal_getChar( ch ) )

void terminal_flush( void );

/** Check if terminal rx task is working. */
bool terminal_isBusy( void );
#define terminal_wBusy( lc ) waitWhile( lc, terminal_isBusy() )

/** It passes a message to terminal rx task to get a string.
  * @param string: Destination of string.
  * @param length: Max. length of string buffer.
  * @retval false while the message is not still passed.
  * @retval true when the message is passed successfully. */
bool terminal_getString( char *string, uint16_t length );
#define terminal_wGetString( lc, string, length )  \
    waitUntil( lc, terminal_getString( string, length ) )

/** It passes a message to terminal rx task to get a numerical string.
  * @param string: Destination of string.
  * @param length: Max. length of string buffer.
  * @retval false while the message is not still passed.
  * @retval true when the message is passed successfully. */
bool terminal_getNumeric( char *string, uint16_t length );
#define terminal_wGetNumeric( lc, string, length )  \
    waitUntil( lc, terminal_getNumeric( string, length ) )

/** It passes a message to terminal rx task to get a hexa digits string.
  * @param string: Destination of string.
  * @param length: Max. length of string buffer.
  * @retval false while the message is not still passed.
  * @retval true when the message is passed successfully. */
bool terminal_getHex( char *string, uint16_t length );



/* ------------------------------------------------------------------------ */
/* -------------------------------------------------------------- Send: --- */
/* ------------------------------------------------------------------------ */

/** This task gets data from an output queue, it
  * formats them to text and sends by serial port. */
state_t terminal_txTask( void );

/** Try to put a pointer to string in output queue.
  * @param msg: Pointer to string.
  * @retval true: If the pointer is put successfully.
  * @retval false: While there is not enough space in output queue. */
bool terminal_str( const char *msg );
#define terminal_wStr( lc, msg ) waitUntil( lc, terminal_str( msg ) )

bool terminal_backSpaces( uint8_t qty );
#define terminal_wBackSpaces( lc, qty ) \
    waitUntil( lc, terminal_backSpaces( qty ) )

/** Try to put a char in output queue.
  * @param msg: Pointer to string.
  * @retval true: If the char is put successfully.
  * @retval false: While there is not enough space in output queue. */
bool terminal_char( char ch );
#define terminal_wChar( lc, ch ) waitUntil( lc, terminal_char( ch ) )

/** Try to put a end of line in output queue.
  * @retval true: If the end of line is put successfully.
  * @retval false: While there is not enough space in output queue. */
bool terminal_endl( void );
#define terminal_wEndl( lc ) waitUntil( lc, terminal_endl() )

/** Try to put a byte in output queue to be printed in hex format.
  * @retval true: If the byte is put successfully.
  * @retval false: While there is not enough space in output queue. */
bool terminal_x8( uint8_t byte );
#define terminal_wX8( lc, byte ) waitUntil( lc, terminal_x8( byte ) )

/** Try to put a unsigned byte in output queue to be printed in dec format.
  * @retval true: If the byte is put successfully.
  * @retval false: While there is not enough space in output queue. */
bool terminal_u8( uint8_t byte );
#define terminal_wU8( lc, byte ) waitUntil( lc, terminal_u8( byte ) )

/** Try to put a signed byte in output queue to be printed in dec format.
  * @retval true: If the byte is put successfully.
  * @retval false: While there is not enough space in output queue. */
bool terminal_s8( int8_t byte ); 
#define terminal_wS8( lc, byte ) waitUntil( lc, terminal_s8( byte ) )

/** Try to put a word in output queue to be printed in hex format.
  * @retval true: If the byte is put successfully.
  * @retval false: While there is not enough space in output queue. */
bool terminal_x16( uint16_t word );
#define terminal_wX16( lc, word ) waitUntil( lc, terminal_x16( word ) )

/** Try to put a unsigned word in output queue to be printed in dec format.
  * @retval true: If the word is put successfully.
  * @retval false: While there is not enough space in output queue. */
bool terminal_u16( uint16_t word );
#define terminal_wU16( lc, word ) waitUntil( lc, terminal_u16( word ) )

/** Try to put a signed word in output queue to be printed in dec format.
  * @retval true: If the word is put successfully.
  * @retval false: While there is not enough space in output queue. */
bool terminal_s16( int16_t word );
#define terminal_wS16( lc, word ) waitUntil( lc, terminal_s16( word ) )

/** @ } */

#endif	/* _TERMINAL_ */

