
/* ------------------------------------------------------------------------ */
/** @file   hdr-speeds.h
  * @brief  This file the name of the HDR speeds are defined.
  * @date   04/01/2014.
  * @author Rafa García.                                                    */
/* ------------------------------------------------------------------------ */

#ifndef HDR_SPEEDS_H
#define	HDR_SPEEDS_H

#define _SPEED_00 "1/25"
#define _SPEED_01 "1/20"
#define _SPEED_02 "1/15"
#define _SPEED_03 "1/13"
#define _SPEED_04 "1/10"
#define _SPEED_05 "1/8"
#define _SPEED_06 "1/6"
#define _SPEED_07 "1/5"
#define _SPEED_08 "1/4"
#define _SPEED_09 "0\"3"
#define _SPEED_10 "0\"4"
#define _SPEED_11 "0\"5"
#define _SPEED_12 "0\"6"
#define _SPEED_13 "0\"8"
#define _SPEED_14 "1\""
#define _SPEED_15 "1\"3"
#define _SPEED_16 "1\"6"
#define _SPEED_17 "2\""
#define _SPEED_18 "2\"5"
#define _SPEED_19 "3\"2"
#define _SPEED_20 "4\""
#define _SPEED_21 "5\""
#define _SPEED_22 "6\""
#define _SPEED_23 "8\""
#define _SPEED_24 "10\""
#define _SPEED_25 "13\""
#define _SPEED_26 "15\""
#define _SPEED_27 "20\""
#define _SPEED_28 "25\""
#define _SPEED_29 "30\""

#define _SPEEDS_QTY 30

#endif	/* HDR_SPEEDS_H */

