
/* ------------------------------------------------------------------------ */
/** @file switch.h
  * @brief Interface Switch Driver module.
  * @date 04/01/2014.
  * @author   Rafa García.                                                  */
/* ------------------------------------------------------------------------ */

#ifndef _SWITCH_
#define _SWITCH_

#include <stdbool.h>
#include "scheduler.h"
#include "intervalometer.h"

/** @defgroup switch Switch Driver
  * This modules implements a task that captures actions in switch button.
  * The detected actions are: Short press and long press.
  * Long press stops the inetrvalometer process.
  * Short press run the programmed intervalometer process. @{  */

/** This task captures actions in switch button. */
state_t switch_task( void );

/** Programs running intervalometer process when short press event. */
void switch_setProcess( process_t p );

/** Get the intervalometer process runned when short press event. */
process_t switch_getProcess( void );


/** @ } */

#endif /* _SWITCH_ */
