
/* ------------------------------------------------------------------------ */
/** @file  timer.h
  * @brief Interface Timer Driver module.
  * @date   20/09/2015
  * @author Rafa García.                                                    */
/* ------------------------------------------------------------------------ */

#ifndef _TTMER_
#define _TTMER_

#include <stdbool.h>
#include <stdint.h>

/** @defgroup timer Timer Driver 
  * Module that controls hardware timers so that tasks can make
  * measurements of time. @{  */

/** Frequency of timer ticks. */
#define TIMER_FREQ  100

/** Timestamp. */
typedef uint32_t timer_t;

/** Convert to timer ticks a time in seconds. */
#define __sec( x ) ((timer_t)(TIMER_FREQ*(x))?(timer_t)(TIMER_FREQ*(x)):1)

/** Convert to timer ticks a time in tenths of seconds. */
#define __tenth( x ) ((timer_t)((TIMER_FREQ/10)*(x))?(timer_t)((TIMER_FREQ/10)*(x)):1)

/** Configure this module and hardware timer. */
void timer_config( void );

/** Indicates whether a process needs or not that the timer is running. */
void timer_go( bool go );

/** Get current timestamp. 
  * @return The timestamp in number of timer ticks. */
timer_t timer_getTimeStamp( void );

/** Checks if time has expired.
  * @param timeStamp: Timer tick from when began timing.
  * @param time: Amount of time (in timer ticks) with which it is compared.
  * @return True if time has expired, false otherwise.                      */
bool timer_isTimeOver( timer_t timeStamp, timer_t const period );

/** Checks if period has expired.
  * @param timeStamp:   When the period began. if period has
  *                     expired it is increased by period.
  * @param period:      Length of period in timer ticks.
  * @return True if time has expired, false otherwise.                      */
bool timer_isPeriodOver( timer_t *timeStamp, timer_t const period );

/** @ } */

#endif /* _TTMER_ */


