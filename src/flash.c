
/* ------------------------------------------------------------------------ */
/** @file   flash.c
  * @brief  This module defines functions that erases and programs 
  *         settings of this applicaion in a segment of internal flash memory.
  * @date   04/01/2014.
  * @author Rafa García.                                                    */
/* ------------------------------------------------------------------------ */

#include <msp430.h>
#include <intrinsics.h>
#include "flash.h"

/** Size in bytes of information segment A. */
#define _size 0x40

/** Data in information segment A: */
const union _page_u {
    uint8_t b[_size];
    struct {
        intervalometer_t intervalometer;  /**< intervalometer settings     */    
        process_t switchProcess; /**< process that is runned by swith task */
        process_t alarmProcess;  /**< process that is runned by rtc task   */
    };
} _page __attribute__((section(".infob"))) = { 
    .intervalometer = { 4, 1, 10, 4, 0, 1 },
    .switchProcess = PR_INTER,
    .alarmProcess = PR_INTER
};

/* Erase the setting segment: */
void flash_erase( void ) {

    FCTL2 = FWKEY | FSSEL_2 | 0x02; // Select SMCLK/3 = 1MHz/3 = 333333Hz
    FCTL3 = FWKEY;          // Clear LOCK bit
    FCTL1 = FWKEY | ERASE;  // Set Erase bit, allow interrupts
    ((uint8_t*)(_page.b))[0] = 0;       // Dummy write to erase Flash seg  
    FCTL1 = FWKEY;          // Clear WRT bit
    FCTL3 = FWKEY|LOCKA|LOCK;  // Set LOCK bit   
}

/* Save the application settings in internal flash memory: */
void flash_save( intervalometer_t const *inetr, process_t sw, process_t alarm ) {
    FCTL3 = FWKEY;         // Clear Lock bit
    FCTL1 = FWKEY+WRT;     // Set WRT bit for write operation 
    for ( unsigned i = 0; i < sizeof(intervalometer_t); i++ )
        ((uint8_t*)&_page.intervalometer)[i] = ((uint8_t*)inetr)[i];    
    *(process_t*)(&_page.switchProcess) = sw;
    *(process_t*)(&_page.alarmProcess) = alarm;    
    FCTL1 = FWKEY;      // Clear WRT bit
    FCTL3 = FWKEY|LOCKA|LOCK; // Set LOCK bit        
}

/* Get from internal flash the intervalometer settings: */
void flash_getInterval( intervalometer_t *intervalometer ) {
    for ( unsigned i = 0; i < sizeof(intervalometer_t); i++ )
        ((uint8_t*)intervalometer)[i] = ((uint8_t*)&_page.intervalometer)[i];    
}

/* Get from internal flash the process that is runned by swith task: */
process_t flash_getSwitchProcess( void ) { return _page.switchProcess; }

/* Get from internal flash the process that is runned by rtc task: */
process_t flash_getAlarmProcess( void ) { return _page.alarmProcess; }

/* ------------------------------------------------------------------------ */
