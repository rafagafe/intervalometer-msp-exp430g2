
/* ------------------------------------------------------------------------ */
/** @file  intervalometer.h
  * @brief Interface Intervalometer module.
  * @date   20/09/2015
  * @author Rafa García.                                                    */
/* ------------------------------------------------------------------------ */

#ifndef _INTERVALOMETER_
#define	_INTERVALOMETER_

#include <stdbool.h>
#include <stdint.h>
#include "scheduler.h"
#include "hdr-speeds.h"

/** @defgroup intervalometer Intervalometer
  * This module implements a task that drives the trigger of the camera.
  * Also implements the methods to comunate with this task. 
  * This task has two modes of operation. Intervalometer one and another
  * performs a sequence of shots to get a HDR.@{  */

typedef enum __attribute__(( packed )) { 
    PR_INTER, /**< A short press runs intervalometer process. */
    PR_HDR    /**< A short press runs HDR process. */
} process_t;

/** Intervalometer task settings. */
typedef struct intervalometer_s {
    uint16_t Delay; /**< This adjusts the time (in seconds)
                         between “start” and the first photo. */
    uint16_t Long;  /**< This affects how long the shutter remains open. 
                         This setting’s only relevant if you’re shooting 
                         in bulb mode and has not effect in HDR process. */
    uint16_t Intv;  /**< How long between shutter clicks. */
    uint16_t Numb;  /**< The total number of shots. */
    uint8_t  HDR;   /**< Initial HDR speed. ( 0-> 1/25, 29 -> 30" ). */
    uint8_t  step;  /**< 1/3 EV increases in HDR process. */
} intervalometer_t;

/** This task drives the trigger of the camera. */
state_t intervalometer_task( void );

/** Set intervalometer parameters.
  * @param data[in]: Parameter values source.
  * @retval false: While intervalometer task is running a process. 
  * @retval true: When the paramters are set successfully.  */
bool intervalometer_set( intervalometer_t const* data );

/** Get the current intervalometer parameters.
  * @param data[out]: Parameter struct destination. */
void intervalometer_get( intervalometer_t* data );

/** Try to pass a message to intervalometer task to run a process. 
  * @retval true: When the message is sent successfully.
  * @retval false: While intervlometer task is running a process. */
bool intervalometer_run( process_t p );

/** Pass a message to intervalometer task to stop any running process. */
void intervalometer_stop( void ); 

/** Check the task interval timer is running some process. */
bool intervalometer_isRunning( void );

/** @ } */

#endif	/* _INTERVALOMETER_ */

