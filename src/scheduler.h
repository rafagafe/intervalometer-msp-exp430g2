
/* ------------------------------------------------------------------------ */
/** @file  scheduler.h
  * @brief Interface to Scheduler module.
  * @date   20/09/2015
  * @author Rafa García.                                                    */
/* ------------------------------------------------------------------------ */

#ifndef _SCHEDULER_
#define _SCHEDULER_

#include "protothread.h"

/** @defgroup scheduler Scheduler  @{  */

typedef state_t(*thread_t)(void);

/** Tell to the scheduler that there is a new pending event. */
void scheduler_wakeup( void );

/** Executes tasks by priority and manages the low power mode. */
void scheduler_run( thread_t const* const pool );

/** @ } */

#endif /* _SCHEDULER_ */


