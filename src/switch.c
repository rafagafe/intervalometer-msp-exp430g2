
/* ------------------------------------------------------------------------ */
/** @file switch.c
  * @brief This modules implements a task that captures actions in switch 
  *        button. The detected actions are: Short press and long press.
  *        Long press stops the inetrvalometer process.
  *        Short press run the programmed intervalometer process.
  * @author   Rafa García.                                                  */
/* ------------------------------------------------------------------------ */

#include <msp430.h>
#include "switch.h"
#include "timer.h"
#include "intervalometer.h"
#include "flash.h"

/** Bit in GPIO port of swith. */
#define SWITCH                  BIT3
#define _isPressed()            (!(P1IN & SWITCH))
#define _isReleased()           (P1IN & SWITCH)
#define _interruptWhenPress()   P1IES |= SWITCH
#define _interruptWhenRelease() P1IES &= ~SWITCH 

/** Process runned when short press event. */
static process_t _process;

/* Programs running intervalometer process when short press: */
void switch_setProcess( process_t p ) { _process = p; }

/* Get the intervalometer process runned when short press event: */
process_t switch_getProcess( void ) { return _process; }

/* This task captures actions in switch button: */
state_t switch_task( void ) {
    
    static lc_t lc = 0;
    static timer_t t;
    
    begin( lc ); 
    
    P1DIR &= ~SWITCH; 
    P1REN |=  SWITCH;  
    P1OUT |=  SWITCH;  
    P1IE  |= SWITCH; 
    _process = flash_getSwitchProcess();
    
    /* main loop: */
    while( true ) { 
        
        /* debounce loop: */
        do {
            _interruptWhenPress();                           
            waitUntil( lc, _isPressed() );
            timer_go( true );
            t = timer_getTimeStamp();             
            _interruptWhenRelease();             
            waitUntil( lc, _isReleased() || timer_isTimeOver( t, __sec(0.01) ) );
            timer_go( false );                    
        } while( _isReleased() ); 
        
        timer_go( true );
        t = timer_getTimeStamp(); 
        waitUntil( lc, _isReleased() || timer_isTimeOver( t, __sec(1.0) ) ); 
        timer_go( false );
        
        /* It's been a short pressing: */
        if ( _isReleased() )
            intervalometer_run( _process );          
              
        /* It's being a long pressing: */
        else {
            intervalometer_stop();
            waitUntil( lc, _isReleased() );             
        }

    }
    end( lc );    
}

/** Port 1 ISR: */  
__attribute__( ( __interrupt__( PORT1_VECTOR ), wakeup ) ) 
static void _port1_isr(void) {  
    P1IFG &= ~SWITCH;
    scheduler_wakeup();
}

/* ------------------------------------------------------------------------ */







