
/* ------------------------------------------------------------------------ */
/** @file fifo.c
 * @brief  Defines a structure and functions able to handle byte queues.
 *         The queue size is configurable individually.
 * @author   Rafa García.                                                   */
/* ------------------------------------------------------------------------ */

#include "fifo.h"

/* Checks if queue is full: */
bool fifo_isFull(fifo_t const *fifo) {
  return (fifo->quantity >= fifo->size);
}

/* Checks if queue is empty: */
bool fifo_isEmpty(fifo_t const *fifo) {
  return !fifo->quantity;
}

/* Checks if queue is not full: */
bool fifo_isNotFull(fifo_t const *fifo) {
  return (fifo->quantity < fifo->size);
}

/* Gets the quantity of bytes of data in queue: */
bool fifo_isNotEmpty(fifo_t const *fifo) {
  return (bool) (fifo->quantity);
}

/* Gets the quantity of bytes of data in queue: */
unsigned fifo_getDataQty(fifo_t const *fifo) {
    return fifo->quantity;
}

/* Gets the size of the free space in bytes: */
unsigned fifo_getSpaceAvilable(fifo_t const *fifo) {
    return (fifo->size - fifo->quantity);
}

/* Empty the queue: */
void fifo_flush(fifo_t *fifo) {
    fifo->first = fifo->last = fifo->quantity = 0;
}

/* Puts a byte into the queue: */
bool fifo_put(fifo_t *fifo, uint8_t byte) {
    if (fifo_isFull(fifo)) return false;
    fifo->bytes[fifo->last] = byte;
    if (++fifo->last >= fifo->size) fifo->last = 0;
    fifo->quantity++;
    return true;
}

/* Gets a byte from queue: */
bool fifo_get(fifo_t *fifo, uint8_t *byte) {
    if (fifo_isEmpty(fifo)) return false;
    *byte = fifo->bytes[fifo->first];
    if (++fifo->first >= fifo->size) fifo->first = 0;
    fifo->quantity--;
    return true;
}

/* Puts an array of byte in the queue: */
bool fifo_putStr( fifo_t *fifo, uint8_t *data, unsigned length ) {
    if ( length > fifo_getSpaceAvilable( fifo ) ) return false;
    unsigned i;
    for (i = 0; i < length; i++) {
        fifo->bytes[fifo->last] = data[i];
        if (++fifo->last >= fifo->size) fifo->last = 0;
    }
    fifo->quantity += length;
    return true;
}

bool fifo_getStr( fifo_t *fifo, uint8_t *data, unsigned length ) {
    if ( length > fifo_getDataQty( fifo ) ) return false;
    unsigned i;
    for (i = 0; i < length; i++) {
        data[i] = fifo->bytes[fifo->first];
        if ( ++fifo->first >= fifo->size ) fifo->first = 0;
    }
    fifo->quantity -= length;
    return true;
}

/* Puts a pointer in the queue: */
bool fifo_putPtr( fifo_t *fifo, void *ptr ) {
    return fifo_putStr(fifo, (uint8_t*)&ptr, sizeof(void*) );
}

/* Gets a pointer from queue: */
void* fifo_getPtr( fifo_t *fifo ) {
    void *retVal = 0;
    fifo_getStr(fifo, (uint8_t*)&retVal, sizeof(void*) );
    return retVal;
}
/* ------------------------------------------------------------------------ */







