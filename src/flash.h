
/* ------------------------------------------------------------------------ */
/** @file   flash.h
  * @brief  Interface to Internal Flash module.
  * @date   04/01/2014.
  * @author Rafa García.                                                    */
/* ------------------------------------------------------------------------ */

#ifndef _FLASH_
#define	_FLASH_

#include "intervalometer.h"

/** @defgroup flash Internal Flash. 
  * Defines functions that erases and programs settings of this applicaion
  * in a segment of internal flash memory.  @{  */
 
/** Erase the setting segment. */
void flash_erase( void );

/** Save the application settings in internal flash memory.
  * @param inetr: Intervalometer setting,
  * @param sw: Process that is runned by swith task.
  * @param alarm: Process that is runned by rtc task. */
void flash_save( const intervalometer_t *inetr, process_t sw, process_t alarm );

/** Get from internal flash the intervalometer settings. */
void flash_getInterval( intervalometer_t *intervalometer );

/** Get from internal flash the process that is runned by swith task. */
process_t flash_getSwitchProcess( void );

/** Get from internal flash the process that is runned by rtc task. */
process_t flash_getAlarmProcess( void );

/** @} */
#endif	/* _FLASH_ */

