
/* ------------------------------------------------------------------------ */
/** @file   fifo.h
  * @brief  Interface FIFO module.
  * @date   04/01/2014.
  * @author Rafa García.                                                    */
/* ------------------------------------------------------------------------ */

#ifndef _FIFO_
#define _FIFO_

#include <stdbool.h>
#include <stdint.h>

/** @defgroup fifo FIFO 
  * Defines a structure and functions able to handle byte queues.
  * The queue size is configurable individually.
  * @{  */

/** Structure capable of storing a byte queue. */
typedef struct fifo_s {
    unsigned first;        /**< Index of the first byte of the queue. */
    unsigned last;         /**< Index of free space after the last byte. */
    unsigned quantity;     /**< Quantity of bytes in the queue. */
    const unsigned size;   /**< Must be initialized to the size of the array. */
    uint8_t * const bytes; /**< Points to the array of bytes. */
} fifo_t;

/** Checks if queue is full.
  * @param fifo: Reference to queue.
  * @return True if full, false in otherwise. */
bool fifo_isFull( fifo_t const *fifo );

/** Checks if queue is empty.
  * @param fifo: Reference to queue.
  * @return True if empty, false in otherwise. */
bool fifo_isEmpty( fifo_t const *fifo );

/** Checks if queue is not full.
  * @param fifo: Reference to queue.
  * @return True if not full, false in otherwise. */
bool fifo_isNotFull( fifo_t const *fifo );

/** Checks if queue is not empty.
  * @param fifo: Reference to queue.
  * @return True if not empty, false in otherwise. */
bool fifo_isNotEmpty( fifo_t const *fifo );

/** Gets the quantity of bytes of data in queue.
  * @param fifo: Reference to queue.
  * @return Quantity of bytes. */
unsigned fifo_getDataQty( fifo_t const *fifo );

/** Gets the size of the free space in bytes.
  * @param fifo: Reference to queue.
  * @return Bytes quantity. */
unsigned fifo_getSpaceAvilable( fifo_t const *fifo );

/** Empty the queue.
  * @param fifo: Reference to queue. */
void fifo_flush( fifo_t *fifo );

/** Puts a byte in the queue.
  * @param fifo: Reference to queue.
  * @param byte: Value of byte.
  * @return True if put, false in otherwise. */
bool fifo_put( fifo_t *fifo, uint8_t byte );

/** Gets a byte from queue.
  * @param fifo: Reference to queue.
  * @param byte: Destination byte.
  * @return True if got, false in otherwise. */
bool fifo_get( fifo_t *fifo, uint8_t *byte );

/** Puts an array of byte in the queue.
  * @param fifo: Reference to queue.
  * @param data: Reference to array,
  * @param length: Size of array.
  * @return True if put, false in otherwise. */
bool fifo_putStr( fifo_t *fifo, uint8_t *data, unsigned length );

/** Puts a pointer in the queue.
  * @param fifo: Reference to queue.
  * @param ptr: Pointer.
  * @return True if put, false in otherwise. */
bool fifo_putPtr( fifo_t *fifo, void *ptr );

/** Gets a pointer from queue.
  * @param fifo: Reference to queue.
  * @return The pointer if got, null in otherwise. */
void* fifo_getPtr( fifo_t *fifo );

/** @} */
#endif /* _FIFO_ */