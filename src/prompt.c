
/* ------------------------------------------------------------------------ */
/** @file prompt.c
  * @date 04/01/2014.
  * @author   Rafa García.                                                  */
/* ------------------------------------------------------------------------ */

#include <string.h>
#include <stdlib.h>
#include <msp430.h>
#include "prompt.h"
#include "terminal.h"
#include "intervalometer.h"
#include "switch.h"
#include "date-time.h"
#include "rtc.h"
#include "flash.h"

/* ------------------------------------------------------------------------ */
/* ------------------------------------------------------- Definitions: --- */
/* ------------------------------------------------------------------------ */
/** Length of destination buffer for command line. */
#define _LINE_LENGTH   12

/** List of name speeds for hdr process. */
#define _SPEEDS {   _SPEED_00, _SPEED_01, _SPEED_02, _SPEED_03, \
                    _SPEED_04, _SPEED_05, _SPEED_06, _SPEED_07, \
                    _SPEED_08, _SPEED_09, _SPEED_10, _SPEED_11, \
                    _SPEED_12, _SPEED_13, _SPEED_14, _SPEED_15, \
                    _SPEED_16, _SPEED_17, _SPEED_18, _SPEED_19, \
                    _SPEED_20, _SPEED_21, _SPEED_22, _SPEED_23, \
                    _SPEED_24, _SPEED_25, _SPEED_26, _SPEED_27, \
                    _SPEED_28, _SPEED_29                        \
}

/** List of string length of name speeds. */
#define _SIZES {                                                    \
    sizeof(_SPEED_00)-1, sizeof(_SPEED_01)-1, sizeof(_SPEED_02)-1,  \
    sizeof(_SPEED_03)-1, sizeof(_SPEED_04)-1, sizeof(_SPEED_05)-1,  \
    sizeof(_SPEED_06)-1, sizeof(_SPEED_07)-1, sizeof(_SPEED_08)-1,  \
    sizeof(_SPEED_09)-1, sizeof(_SPEED_10)-1, sizeof(_SPEED_11)-1,  \
    sizeof(_SPEED_12)-1, sizeof(_SPEED_13)-1, sizeof(_SPEED_14)-1,  \
    sizeof(_SPEED_15)-1, sizeof(_SPEED_16)-1, sizeof(_SPEED_17)-1,  \
    sizeof(_SPEED_18)-1, sizeof(_SPEED_19)-1, sizeof(_SPEED_20)-1,  \
    sizeof(_SPEED_21)-1, sizeof(_SPEED_22)-1, sizeof(_SPEED_23)-1,  \
    sizeof(_SPEED_24)-1, sizeof(_SPEED_25)-1, sizeof(_SPEED_26)-1,  \
    sizeof(_SPEED_27)-1, sizeof(_SPEED_28)-1, sizeof(_SPEED_29)-1   \
}

/** Command line. */
typedef struct command_s {
    char const* line;               /**< Indentifier. */
    char const* descript;           /**< Explains what command do. */
    state_t(* const thread)(lc_t*); /**< Pointer to spawned trhead of command. */
} command_t;



/* ------------------------------------------------------------------------ */
/* ----------------------------------------------- Internal prototypes: --- */
/* ------------------------------------------------------------------------ */
static state_t _help( lc_t *lc ) ;
static state_t _clear( lc_t *lc );
static state_t _reset( lc_t *lc );
static state_t _run( lc_t *lc );
static state_t _hdr( lc_t *lc );
static state_t _stop( lc_t *lc );
static state_t _set( lc_t *lc );
static state_t _get( lc_t *lc );
static state_t _sw_int( lc_t *lc );
static state_t _sw_hdr( lc_t *lc );
static state_t _time( lc_t *lc );
static state_t _alarm( lc_t *lc );
static state_t _time_set( lc_t *lc );
static state_t _alarm_set( lc_t *lc );
static state_t _alarm_off( lc_t *lc );
static state_t _alarm_int( lc_t *lc );
static state_t _alarm_hdr( lc_t *lc );
static state_t _rtc_off( lc_t *lc );
static state_t _save( lc_t *lc );


/* ------------------------------------------------------------------------ */
/* ----------------------------------------------- Internal constants : --- */
/* ------------------------------------------------------------------------ */
/** Welcome note.*/
static const char *_welcome = 
" _       _                       _                      _            \n\r"
"(_)_ __ | |_ ___ _ ____   ____ _| | ___  _ __ ___   ___| |_ ___ _ __ \n\r"
"| | '_ \\| __/ _ \\ '__\\ \\ / / _` | |/ _ \\| '_ ` _ \\ / _ \\ __/ _ \\ '__|\n\r"
"| | | | | ||  __/ |   \\ V / (_| | | (_) | | | | | |  __/ ||  __/ |   \n\r"
"|_|_| |_|\\__\\___|_|    \\_/ \\__,_|_|\\___/|_| |_| |_|\\___|\\__\\___|_|   \n\r"
"                                         _  _  _____  ___  \n\r"
" _ __ ___  ___ _ __       _____  ___ __ | || ||___ / / _ \\ \n\r"
"| '_ ` _ \\/ __| '_ \\ ___ / _ \\ \\/ / '_ \\| || |_ |_ \\| | | |\n\r"
"| | | | | \\__ \\ |_) |___|  __/>  <| |_) |__   _|__) | |_| |\n\r"
"|_| |_| |_|___/ .__/     \\___/_/\\_\\ .__/   |_||____/ \\___/ \n\r"
"              |_|                 |_|                      \n\r";

/** Command line list. */
static const command_t _commands[] = {
    { "help", "Print this text.", _help },
    { "clear", "Clear screen.", _clear },
    { "reset", "Reset the device.", _reset },
    { "run", "Run intervalometer.", _run },
    { "hdr", "Run HDR process.", _hdr },
    { "stop", "Stop intervalometer.", _stop },
    { "set", "Set intervalometer parameters.", _set },
    { "get", "Print intervalometer parameters.", _get },
    { "sw-int", "Set switch short pressing function as run intervalometer.", _sw_int },
    { "sw-hdr", "Set switch short pressing function as run HDR.", _sw_hdr },
    { "time", "Print the time.", _time },
    { "time-set", "Set the date and time.", _time_set },
    { "time-off", "Turn off RTC.", _rtc_off },
    { "alarm", "Print the alarm time.", _alarm },
    { "alarm-set", "Set the date and time of the alarm.", _alarm_set },
    { "alarm-off", "Disable any pending alarm.", _alarm_off },
    { "alarm-int", "Sets alarm event function as run intervalometer.", _alarm_int },
    { "alarm-hdr", "Sets alarm event function as run HDR.", _alarm_hdr },
    { "save", "Save settings in flash.", _save },
    { 0 }
};

/** Name of month for printing. */
static const char* _month[] = {
    "January", "February", "March", "April", "May", "June", "July", 
    "August", "September", "October", "November", "December"
};

/** List of name speeds for hdr process. */
static char const* _speeds[] = _SPEEDS;

/** List of string length od name speeds. */
static uint8_t const _sizes[] = _SIZES;

/** Buffer for comand line transfer. */
static char _buffer[_LINE_LENGTH];

/** Shared memory for spawned thread. */
static union {
    uint16_t index; /**< Used in help command. */
    intervalometer_t data; /**< Used in intervalometer commands. */  
    struct {
        lc_t child;
        dateTime_t dt; 
    } t; /**< Used in rtc commands. */  
}_vars;


/* This task executes commands line. */
state_t prompt_task( void ) {
    static lc_t lc = 0;
    static lc_t ch = 0; 
    static command_t const*command;
    begin( lc ); 
    terminal_wOpen( lc );    
    terminal_wStr( lc, "\x1B[2J" );
    terminal_wStr( lc, _welcome );
    terminal_wEndl( lc );
    while( true ) {        
        terminal_wStr( lc, "$ " );        
        terminal_wGetString( lc, _buffer, _LINE_LENGTH );
        terminal_wBusy( lc );        
        if ( _buffer[0] == '\0' ) continue;
        for( command = _commands; command->line; command++ )
            if ( !strcmp( command->line, _buffer ) ) {
                spawn( lc, ch, command->thread( &ch ) );
                break;
            }
            else yield( lc );
        if ( !command->line )
            terminal_wStr( lc, "Unknown command.\n\r" );
    }
    end( lc );
}

/** Command that print the usage. */
static state_t _help( lc_t *lc ) {
    begin( *lc );
    for( _vars.index = 0; _commands[_vars.index].line; _vars.index++ ) {
        terminal_wStr( *lc, _commands[_vars.index].line );
        terminal_wStr( *lc,  ": " );
        terminal_wStr( *lc, _commands[_vars.index].descript );
        terminal_wEndl( *lc );
    }
    terminal_wStr( *lc, "Note: Switch long pressing function is set as stop.\n\r" );
    terminal_wStr( *lc, "Get code: https://bitbucket.org/rafagafe/intervalometer-msp-exp430g2/\n\r");
    end( *lc );
}

/** Command that clears de screen. */
static state_t _clear( lc_t *lc ) {
    begin( *lc );
    terminal_wStr( *lc, "\x1B[2J" );
    end( *lc );
}

/** Reset the device. */
static state_t _reset( lc_t *lc ) {
    begin( *lc ); 
    ((void(*)(void))(RESET_VECTOR))();
    end( *lc );
}
/** Command that runs intervalometer process.  */
static state_t _run( lc_t *lc ) {
    begin( *lc );
    if ( !intervalometer_run( PR_INTER ) )
        terminal_wStr( *lc, "Intervalometer is already running.\n\r" );
    end( *lc );
}

/** Command that runs hdr process.  */
static state_t _hdr( lc_t *lc ) {
    begin( *lc );
    if ( !intervalometer_run( PR_HDR ) )
        terminal_wStr( *lc,  "Intervalometer is already running.\n\r" );
    end( *lc );
}

/** Command that stops any running process. */
static state_t _stop( lc_t *lc ) {
    begin( *lc );
    intervalometer_stop();
    end( *lc );
}

/** Command that sets intervalometer parameters. */  
static state_t _set( lc_t *lc ) {
    begin( *lc );  
    if ( intervalometer_isRunning() )         
        terminal_wStr( *lc, "Intervalometer is running.\n\r" );
    else {              
        terminal_wStr( *lc, "Enter intervalometer parameters:\n\r" );
        terminal_wStr( *lc, "        Delay, (seconds): " );
        terminal_wGetNumeric( *lc, _buffer, 5 );
        terminal_wBusy( *lc ) ; 
        _vars.data.Delay = atoi( _buffer );
        yield( *lc );
        terminal_wStr( *lc, "         Long, (seconds): " );
        terminal_getNumeric( _buffer, 5 );
        terminal_wBusy( *lc ) ; 
        _vars.data.Long = atoi( _buffer );
        terminal_wStr( *lc, "         Intv, (seconds): " );
        terminal_getNumeric( _buffer, 5 );
        terminal_wBusy( *lc ) ; 
        _vars.data.Intv = atoi( _buffer ); 
        terminal_wStr( *lc, "                       N: " );
        terminal_getNumeric( _buffer, 5 );
        terminal_wBusy( *lc ) ; 
        _vars.data.Numb = atoi( _buffer ); 
        terminal_wStr( *lc, "      HDR, (+, -, enter): " );
        _vars.data.HDR = 0;        
        terminal_wStr( *lc, _speeds[_vars.data.HDR] );
        terminal_flush();        
        do {            
            terminal_wGetChar( *lc, _buffer );
            if ( (_buffer[0] == '+') || (_buffer[0] == '-') ) {
                terminal_wBackSpaces( *lc, _sizes[_vars.data.HDR] );                
                if ( _buffer[0] == '+' ) { if ( _vars.data.HDR >= _SPEEDS_QTY-1 ) _vars.data.HDR = 0; else _vars.data.HDR++; }
                else { if ( _vars.data.HDR > 0 ) _vars.data.HDR--; else _vars.data.HDR = _SPEEDS_QTY-1; }                
                terminal_wStr( *lc, _speeds[_vars.data.HDR] );
            }
        } while( (_buffer[0] != '\n')&&(_buffer[0] != '\r') );        
        terminal_wEndl( *lc );              
        _vars.data.step = 1;        
        terminal_wStr( *lc, "     Step, (+, -, enter): +" );
        waitUntil( *lc, terminal_u8( _vars.data.step ) );        
        terminal_wStr( *lc, "/3 EV" );
        terminal_flush();
        do {
            terminal_wGetChar( *lc, _buffer );
            if ( (_buffer[0] == '+') || (_buffer[0] == '-') ) {
                waitUntil( *lc, terminal_backSpaces( 6 ) );  
                if ( _buffer[0] != '-' ) { if ( _vars.data.step >= 5 ) _vars.data.step = 1; else _vars.data.step++; }
                else { if ( _vars.data.step > 1 ) _vars.data.step--; else _vars.data.step = 5; }                                
                terminal_wU8( *lc, _vars.data.step );
                terminal_wStr( *lc, "/3 EV" );
            }
        } while( (_buffer[0] != '\n')&&(_buffer[0] != '\r') );        
        terminal_wEndl( *lc );                
        if ( !intervalometer_set( &_vars.data ) )
            terminal_wStr( *lc, "Intervalometer is running. Try again later or enter \"stop\" command.\n\r" );
    }
    end( *lc );
}

/** Command that prints the current intervalometer parameters. */  
static state_t _get( lc_t *lc ) {
    begin( *lc );
    intervalometer_get( &_vars.data );
    terminal_wStr( *lc, "Delay: " );
    terminal_wU16( *lc, _vars.data.Delay );
    terminal_wStr( *lc, "\"\n\rLong:  " );    
    terminal_wU16( *lc, _vars.data.Long );
    terminal_wStr( *lc, "\"\n\rIntv:  " );
    terminal_wU16( *lc, _vars.data.Intv );
    terminal_wStr( *lc, "\"\n\rN:     " );
    terminal_wU8( *lc, _vars.data.Numb );
    terminal_wStr( *lc, "\n\rHDR:   " );
    terminal_wStr( *lc, _speeds[_vars.data.HDR] );
    terminal_wStr( *lc, "\n\rStep:  +" );    
    terminal_wU8( *lc, _vars.data.step );
    terminal_wStr( *lc, "/3 EV\n\r" );
    end( *lc );
}    

/** Command that sets switch short pressing function as run intervalometer. */
static state_t _sw_int( lc_t *lc ) {
    begin( *lc );
    switch_setProcess( PR_INTER );
    end( *lc );
}

/** Command that sets switch short pressing function as run HDR. */
static state_t _sw_hdr( lc_t *lc ) {
    begin( *lc );
    switch_setProcess( PR_HDR );
    end( *lc );
}

/** Spawned thread that prints a date-time. */
static state_t _printDateTime( lc_t *lc ) {
    begin( *lc );
    if ( _vars.t.dt.hour < 10 ) terminal_wChar( *lc, '0' );
    terminal_wU8( *lc, _vars.t.dt.hour );
    terminal_wChar( *lc, ':' );
    if ( _vars.t.dt.min < 10 ) terminal_wChar( *lc, '0' );
    terminal_wU8( *lc, _vars.t.dt.min );
    terminal_wChar( *lc, '.' );
    if ( _vars.t.dt.sec < 10 ) terminal_wChar( *lc, '0' );
    terminal_wU8( *lc, _vars.t.dt.sec );
    terminal_wChar( *lc, ' ' );
    terminal_wStr( *lc, _month[_vars.t.dt.month] );
    terminal_wChar( *lc, ' ' );
    terminal_wU8( *lc, _vars.t.dt.day );
    terminal_wStr( *lc, ", " );
    terminal_wU16( *lc, dateTime_getYear( &_vars.t.dt ) );
    terminal_wEndl( *lc );   
    end( *lc );    
}

/** Command that prints the current time. */
static state_t _time( lc_t *lc ) {
    begin( *lc );
    rtc_get( &_vars.t.dt );
    spawn( *lc, _vars.t.child, _printDateTime( &_vars.t.child ) );
    end( *lc );    
}

/** Command that prints the alarm register. */
static state_t _alarm( lc_t *lc ) {
    begin( *lc );
    rtc_getAlarm( &_vars.t.dt );
    spawn( *lc, _vars.t.child, _printDateTime( &_vars.t.child ) );
    end( *lc );    
}

/** Spawned thread that sets a date-time. */
static state_t _setDateTime( lc_t *lc ) {
    begin( *lc ); 
    _vars.t.dt.hour =_vars.t.dt.min = _vars.t.dt.sec = 0;
    terminal_wStr( *lc, "Year: " );
    terminal_wGetNumeric( *lc, _buffer, 5 );    
    terminal_wBusy( *lc ); 
    dateTime_setYear( &_vars.t.dt, atoi( _buffer ) );
    do {
        terminal_wStr( *lc, "Month: " );
        terminal_getNumeric( _buffer, 3 );
        terminal_wBusy( *lc ) ; 
        _vars.t.dt.month = atoi( _buffer ) - 1; 
    } while( _vars.t.dt.month > december );     
    do {   
        terminal_wStr( *lc, "Day: " );
        terminal_getNumeric( _buffer, 3 );
        terminal_wBusy( *lc ) ; 
        _vars.t.dt.day = atoi( _buffer );
    } while( !dateTime_check( &_vars.t.dt ) );  
    do {
        terminal_wStr( *lc, "Hour: " );
        terminal_getNumeric( _buffer, 3 );
        terminal_wBusy( *lc ) ; 
        _vars.t.dt.hour = atoi( _buffer );
    } while( _vars.t.dt.hour > 23 );
    do {
        terminal_wStr( *lc, "Minute: " );
        terminal_getNumeric( _buffer, 3 );
        terminal_wBusy( *lc ) ; 
        _vars.t.dt.min = atoi( _buffer );
    } while( _vars.t.dt.min > 59 );
    do {
        terminal_wStr( *lc, "Second: " );
        terminal_getNumeric( _buffer, 3 );
        terminal_wBusy( *lc ) ; 
        _vars.t.dt.sec = atoi( _buffer );
    } while( _vars.t.dt.sec > 59 );    
    end( *lc );    
}    

/** Command that sets the date and time and run RTC. */
static state_t _time_set( lc_t *lc ) {
    begin( *lc );
    spawn( *lc, _vars.t.child, _setDateTime( &_vars.t.child ) );
    if ( rtc_set( &_vars.t.dt ) ) 
        terminal_wStr( *lc, "RTC is running.\n\r" );
    else
        terminal_wStr( *lc, "Error.\n\r" );
    end( *lc );    
}    

/** Command that sets the date and time of the alarm. */
static state_t _alarm_set( lc_t *lc ) {
    begin( *lc );
    spawn( *lc, _vars.t.child, _setDateTime( &_vars.t.child ) );
    if ( rtc_setAlarm( &_vars.t.dt ) )
        terminal_wStr( *lc, "Alarm On.\n\r" );
    else 
        terminal_wStr( *lc, "Error.\n\r" );
    end( *lc );    
}  

/** Command that disable any pending alarm. */
static state_t _alarm_off( lc_t *lc ) {
    begin( *lc ); 
    rtc_alarm( false );
    end( *lc );        
}

/** Command that sets alarm event function as run intervalometer. */
static state_t _alarm_int( lc_t *lc ) {
    begin( *lc );
    rtc_setAlarmProcess( PR_INTER );
    end( *lc );
}

/** Command that sets alarm event function as run HDR. */
static state_t _alarm_hdr( lc_t *lc ) {
    begin( *lc );
    rtc_setAlarmProcess( PR_HDR );
    end( *lc );
}

/** Command that turns off RTC. */
static state_t _rtc_off( lc_t *lc ) {
    begin( *lc ); 
    rtc_enable( false );
    end( *lc );        
}

/** Save settings in flash. */
static state_t _save( lc_t *lc ) {
    begin( *lc ); 
    flash_erase();
    yield( *lc );
    intervalometer_get( &_vars.data );
    flash_save( &_vars.data, switch_getProcess(), rtc_getAlarmProcess() );    
    end( *lc );        
}

/* ------------------------------------------------------------------------ */







