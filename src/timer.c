
/* ------------------------------------------------------------------------ */
/** @file timer.c
  * @brief This module that controls hardware timers so that tasks can make
  * measurements of time.
  * @date   10/02/2015.
  * @author Rafa García.                                                    */
/* ------------------------------------------------------------------------ */

#include <msp430.h>
#include <intrinsics.h>
#include "timer.h"
#include "scheduler.h"

#define TIMERT_MAX UINT32_MAX

#define TIMER_CLK_SOURCE_FREQUENCY 1000000ULL


#if (((TIMER_CLK_SOURCE_FREQUENCY/(1ULL*TIMER_FREQ))+1) <= 0xffffULL)
#define ID_X          ID_0
#define TACCR0_VALUE  ((TIMER_CLK_SOURCE_FREQUENCY/(1ULL*TIMER_FREQ))+1)
#elif (((1ULL*TIMER_CLK_SOURCE_FREQUENCY/(2ULL*TIMER_FREQ))+1) <= 0xffffULL)
#define ID_X          ID_1
#define TACCR0_VALUE  ((TIMER_CLK_SOURCE_FREQUENCY/(2ULL*TIMER_FREQ))+1)
#elif (((1ULL*TIMER_CLK_SOURCE_FREQUENCY/(4ULL*TIMER_FREQ))+1) <= 0xffffULL)
#define ID_X          ID_2
#define TACCR0_VALUE  ((TIMER_CLK_SOURCE_FREQUENCY/(4ULL*TIMER_FREQ))+1)
#elif (((1ULL*TIMER_CLK_SOURCE_FREQUENCY/(8ULL*TIMER_FREQ))+1) <= 0xffffULL)
#define ID_X          ID_3
#define TACCR0_VALUE  ((TIMER_CLK_SOURCE_FREQUENCY/(8ULL*TIMER_FREQ))+1)
#else
#error "The configurations of portCLK_SOURCE_FREQUENCY_HZ and configTICK_RATE_HZ are not possible."
#define ID_X          ID_0
#endif

/** Enable counter */
static unsigned _go;

/** Ticks counter. */
static volatile timer_t _time;

/* Configure this module and hardware timer. */
void timer_config( void ) {
    _go = 0;
    CCTL0 = CCIE;
    CCR0 = TACCR0_VALUE; 
    TACTL = TASSEL_2 | ID_X;
}

/* Indicates whether a process needs or not that the timer is running. */
void timer_go( bool en ) {
    if ( en ) {
        if( !_go ) TACTL |= MC_1 | TACLR;
        ++_go;        
    }
    else {        
        if ( _go )  --_go;
        if ( !_go ) TACTL &= ~MC_1;
    }
}

/* Get current timestamp. */
timer_t timer_getTimeStamp( void ) { return _time; }

/** Checks if a timer tick is later than another timer tick.
  * @retval true a >= b
  * @retval false a < b  */
static bool _isOver( timer_t a, timer_t b ) {
    return ( (a - b) > (0xfffffffful>>1) )? false: true;
}

/* Checks if time has expired. */
bool timer_isTimeOver( timer_t timeStamp, timer_t time ) {
    return _isOver( _time, time + timeStamp );
}

/* Checks if period has expired. */
bool timer_isPeriodOver( timer_t *timeStamp, timer_t period ) {
    if ( timer_isTimeOver( *timeStamp, period ) ) {
        *timeStamp += period;
        return true;
    }
    return false;    
}

/** Periodic ISR. */
__attribute__( ( __interrupt__( TIMER0_A0_VECTOR ), wakeup ) )
static void _timerA0_isr (void) {
    ++_time;
    scheduler_wakeup();
}

/* ------------------------------------------------------------------------ */
