/* ------------------------------------------------------------------------ */
/** @file protothread.h
  * @brief Protothread framework.
  * @date 04/01/2014.
  * @author   Rafa García.                                                  */
/* ------------------------------------------------------------------------ */

#ifndef _PT_
#define _PT_

/** State of thread. */
typedef enum { 
    BLOCKED = 0, 
    YIELDED = 1, 
    WAITING = 2, 
    EXITED  = 3,
    ENDED   = 4
} state_t;

/** Local continuation. */
typedef void* lc_t;

#define ___concat2(s1, s2) s1##s2
#define ___concat(s1, s2) ___concat2(s1, s2)
#define ___label ___concat(LABEL_, __LINE__)  

#define begin( st ) \
    if ( st != 0 )  \
        goto *st;

#define end( st ) return ENDED

#define waitWhile( st, x ) do {	\
    if ( x ) {                  \
        st = &&___label;        \
        return WAITING;         \
        ___label:               \
        if ( x )                \
            return BLOCKED;     \
    }                           \
} while(0)

#define waitUntil( st, x ) waitWhile( st, !(x) )

#define yield( st ) do {    \
    st = &&___label;        \
    return YIELDED;         \
    ___label: while(0);     \
} while(0)

#define yieldWhile( st, x ) do {    \
    st = &&___label;                \
    ___label:                       \
    if ( x )                        \
        return YIELDED;             \
}while(0)

#define yieldUntil( st, x ) yieldWhile( st, !(x) )

#define restart( st ) do { st = 0; return WAITING; } while(0)

#define spawn( st, ch, func ) do {  \
    state_t ___state;               \
    st = &&___label;                \
    ch = 0;                         \
    ___label:                       \
    ___state = func;                \
    if ( ___state <= WAITING )      \
        return ___state;            \
} while(0)

#endif /* _PT_ */
