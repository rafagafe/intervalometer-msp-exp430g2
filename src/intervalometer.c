
/* ------------------------------------------------------------------------ */
/** @file  intervalometer.c
  * @brief This module implements a task that drives the trigger of the camera.
  * Also implements the methods to comunate with this task. 
  * This task has two modes of operation. Intervalometer one and another
  * performs a sequence of shots to get a HDR.
  * @date   20/09/2015
  * @author Rafa García.                                                    */
/* ------------------------------------------------------------------------ */

#include <stdlib.h>
#include <msp430.h>
#include "intervalometer.h"
#include "timer.h"
#include "terminal.h"
#include "flash.h"

/** LED red of msp-exp430 board. */
#define LED_RED     BIT0 
/** LED green of msp-exp430 board. */
#define LED_GREEN   BIT6
/** Output focus */
#define LED_FOCUS   LED_GREEN
/** Output shut */
#define LED_SHUT    LED_RED
#define LED_OUT     P1OUT
#define LED_DIR     P1DIR

/** Commands to process by inervalometer task. */
typedef enum __attribute__((packed)) command_e { 
    CMD_NOTHING,
    CMD_INTER,
    CMD_HDR
} command_t;

static uint16_t const _speeds[] = {
    __sec(1/25.0), __sec(1/20.0), __sec(1/15.0), __sec(1/13.0), 
    __sec(1/10.0), __sec(1/8.0), __sec(1/6.0), __sec(1/5.0), 
    __sec(1/4.0), __sec(0.3), __sec(0.4), __sec(0.5), __sec(0.6), 
    __sec(0.8), __sec(1.0), __sec(1.3), __sec(1.6), __sec(2.0), 
    __sec(2.5), __sec(3.2), __sec(4.0), __sec(5.0), __sec(6.0), 
    __sec(8.0), __sec(10.0), __sec(13.0), __sec(15.0), __sec(20.0), 
    __sec(25.0), __sec(30.0)                                                };


/* ------------------------------------------------------------------------ */
/* --------------------------------------------------- State variables: --- */
/* ------------------------------------------------------------------------ */
/** Commands to process by inervalometer task. */
static command_t _command;
/** If true tndicates to inetrvalometer that stops any running process. */
static bool _stop;
/** Timer ticks for delay time. */
static timer_t _delay;
/** Timer ticks for long time. */
static timer_t _long;
/** Timer ticks for interval time. */
static timer_t _intv;
/** Timer ticks for focus time first picture. */
static timer_t _firstFocus;
/** Timer ticks for focus time. */
static timer_t _focus;
/** Intervalometer parameters. */
static intervalometer_t _data;



/* ------------------------------------------------------------------------ */
/* --------------------------------------------------- Public funtions: --- */
/* ------------------------------------------------------------------------ */
/* This task drives the trigger of the camera. */
state_t intervalometer_task( void ) {
    static lc_t lc = 0;
    static timer_t t;
    static uint16_t i;    
    begin( lc );    
    LED_DIR |= (LED_RED + LED_GREEN); 
    LED_OUT &= ~(LED_RED + LED_GREEN); 
    flash_getInterval( &_data );
    intervalometer_set( (intervalometer_t*)&_data );
    _stop = false;  
    while( true ) {       
        if ( _stop ) {
            timer_go( false );
            LED_OUT &= ~(LED_FOCUS | LED_SHUT);
            _stop = false;
        }                                 
        _command = CMD_NOTHING;       
        waitUntil( lc, _command != CMD_NOTHING );  
        if ( _command == CMD_INTER ) _long = __sec( _data.Long ); 
        timer_go( true );
        t = timer_getTimeStamp();              
        waitUntil( lc, _stop || timer_isPeriodOver( &t, _delay ) );    
        if ( _stop ) continue;    
        if ( _firstFocus ) waitUntil( lc, timer_isTimeOver( t, __sec(0.5) ) );
        LED_OUT |= LED_FOCUS;
        waitUntil( lc, _stop || timer_isPeriodOver( &t, _firstFocus ) );          
        if ( _stop ) continue;        
        for( i = 1;; ) {                        
            LED_OUT &= ~LED_FOCUS;
            LED_OUT |= LED_SHUT;   
            if ( _command == CMD_HDR ) _long = (timer_t)_speeds[(_data.step*i)+_data.HDR];
            waitUntil( lc, _stop || timer_isPeriodOver( &t, _long ) ); 
            if ( _stop ) break;            
            LED_OUT &= ~LED_SHUT;            
            if ( ++i > _data.Numb ) break;  
            if ( ( _command == CMD_HDR ) && ( ((_data.step*i)+_data.HDR) >= 30 ) ) break;
            waitUntil( lc, _stop || timer_isPeriodOver( &t, _intv ) );     
            if ( _stop ) break;   
            if ( _focus ) waitUntil( lc, timer_isTimeOver( t, __sec(0.5) ) );
            LED_OUT |= LED_FOCUS;             
            waitUntil( lc, _stop || timer_isPeriodOver( &t, _focus ) );                         
        }                 
        if ( _stop ) continue;               
        LED_OUT &= ~LED_FOCUS;        
        timer_go( false );                       
    }    
    end( lc );
}

/* Set intervalometer parameters. */
bool intervalometer_set( intervalometer_t const* data ) {    
    if ( intervalometer_isRunning() ) return false;    
    for ( size_t i = 0; i < sizeof(intervalometer_t); i++ )
        ((uint8_t*)&_data)[i] = ((uint8_t*)data)[i];            
    if ( data->Delay >= 3 ) {
        _delay = __sec( data->Delay - 3 );
        _firstFocus = __sec( 3 );
    }
    else {
        _firstFocus = __sec( data->Delay );
        _delay = (timer_t)0;
    }      
    if ( data->Intv >= 3 ) {
        _intv = __sec( data->Intv - 3 );
        _focus = __sec( 3 );
    }
    else {
        _focus = __sec( data->Intv );
        _intv = (timer_t)0;
    }       
    return true; 
}

/* Get the current intervalometer parameters. */
void intervalometer_get( intervalometer_t* data ) {
    for ( unsigned i = 0; i < sizeof(intervalometer_t); i++ )
        ((uint8_t*)data)[i] = ((uint8_t*)&_data)[i];    
}

/* Pass a message to intervalometer task to run intervalometer process.  */
bool intervalometer_run( process_t p ) {
    if ( intervalometer_isRunning() ) return false;
    switch( p ) {
        case PR_INTER:  _command = CMD_INTER;   break;
        case PR_HDR:    _command = CMD_HDR;     break;
        default:        _command = CMD_NOTHING;
    }    
    return true;
}

/* Pass a message to intervalometer task to stop any running process. */
void intervalometer_stop( void ) { _stop = intervalometer_isRunning(); }

/* Check the task interval timer is running some process. */
bool intervalometer_isRunning( void ) { return (_command != CMD_NOTHING); }


/* ------------------------------------------------------------------------ */
