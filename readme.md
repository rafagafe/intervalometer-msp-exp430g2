# Intervalometer MSP-430EXPG2
See demo [video](https://youtu.be/aAiDhq_HWlw).
## Requirements
 - msp430-gcc
 - mspdebug
 - MSP-430EXPG2 board.
 - VT100 serial terminal emulator, (minicom is recommended)
## Building/Programming                             
First, connect the MSP-430EXPG2 board via USB. Then, run the following command in the makefile-project folder:
```sh
$ make run
```
Then open a VT100 serial terminal emulator and set to 9600bps. The port in Ubuntu usualy is /dev/ttyACM0. If minicom is installed you can run the following command:
```sh
$ make term
```
You can debug using "debug" rule. If you have not the board you can simulate with "sim" rule. The "help" rule shows you all rules:
```sh
$ make help
Targets:
	-build (default)
	-all
	-clean
	-debug
	-sim
	-run
	-term
	-help
$
```

